"use strict"

const ApiGetway = require("moleculer-web")

module.exports ={
    name: "api",
    mixins: [ ApiGetway ],
    settings: {
        port: process.env.PORT || 3000,
        IP: "0.0.0.0",
        routes: [{
            path:"api",
            aliases: {
                "GET food" : "food.get"
            } ,
            //whitelist: [ "**" ],
            logging: true
        }]
    }
}