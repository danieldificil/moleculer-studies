"use strict"

module.exports = {
    name: "food",
    actions: {
        async get()
        {
            const result = await new Promise((resolve,reject) =>
            {
                get("http://taco-food.herokuapp.com/api/v1/food", (res) =>
                {
                res.on("end", ()=> resolve(JSON.parse(res)))
                res.on("error",(error) => reject(error))    
                })
            })
            return result
        }
    }
}